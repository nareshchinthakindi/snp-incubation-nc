package snp;

import java.util.Arrays;

public class RemoveDuplicateCharsInAString {

    public static void main(String[] args) {
        String s = "dabfcadef";

        s.chars().distinct().mapToObj(c -> (char)(c)).forEach(System.out::print);
        System.out.println();
        Arrays.stream(s.split("")).distinct().forEach(System.out::print);
    }
}
