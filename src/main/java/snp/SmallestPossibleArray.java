package snp;

import java.util.Arrays;
import java.util.Comparator;

public class SmallestPossibleArray {

    public static void main(String[] args) {
        int[] arr = {1, 34, 3, 98, 9, 76, 45, 4};


        Arrays.stream(arr)
                .mapToObj(String::valueOf)
                .sorted()
                .forEach(System.out::print);

        System.out.println();
//Highest
        Arrays.stream(arr)
                .mapToObj(String::valueOf)
                .sorted((s1, s2) -> (s2+s1).compareTo(s1+s2))
                .forEach(System.out::print);
    }
}
