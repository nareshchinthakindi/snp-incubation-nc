package snp;

import java.util.Arrays;

public class FindNonRepeatedCharacter {

    public static void main(String[] args) throws Exception {
        String str = "god quick brown fox jumps over the lazy dog";

        System.out.println(str.lastIndexOf('q'));

        System.out.println(str.indexOf('q'));

        String res = Arrays.stream(str.split("")).filter(c -> str.indexOf(c) == str.lastIndexOf(c)).findFirst().orElse("Data No Found");
        System.out.println(res);

      res =  Arrays.stream(str.split("")).filter(c -> str.indexOf(c) == str.lastIndexOf(c)).findFirst().orElseThrow(() -> new Exception("TEst"));
        System.out.println(res);
    }
}
