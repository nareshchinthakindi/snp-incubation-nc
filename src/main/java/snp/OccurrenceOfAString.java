package snp;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class OccurrenceOfAString {

    public static void main(String[] args) {
        String input = "the quick brown fox jumps right over the little lazy dog little";

        Map<String, Long> wordOccurrence = Arrays.stream(input.split(" ")).collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
        System.out.println(wordOccurrence);

        String s = "bbvuunhhaejjj";
        Map<String, Long> charCountWithSort = Arrays.stream(s.split("")).collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
                .entrySet().stream().sorted(Map.Entry.comparingByValue()).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) ->e2, LinkedHashMap::new));
        System.out.println(charCountWithSort);






    }

}
