package snp;

import java.util.Arrays;
import java.util.Comparator;

public class LengthOfLongestWordInaString {
    public static void main(String[] args) {
        String s = "I am interested123455 to grow in my organization";

        System.out.println(Arrays.stream(s.split(" ")).mapToInt(String::length).max().orElse(-1));
        System.out.println(Arrays.stream(s.split(" ")).map(String::length).sorted(Comparator.reverseOrder()).skip(1).findFirst().orElse(-1));
    }
}
