package snp;

import java.util.Arrays;
import java.util.List;

public class SumOfUniqueElements {
    public static void main(String[] args) {
      List<Integer> elements = List.of(5,6,7,8,5,5,8,8,7);

      Integer sum = elements.stream().distinct().mapToInt(Integer::intValue).sum();
        System.out.println(sum);

        int [] array = {5,6,7,8,5,5,8,8,7};

        System.out.println(Arrays.stream(array).distinct().sum());
    }
}
