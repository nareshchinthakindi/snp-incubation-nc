package snp.order;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Order {
    //1L, LocalDate.parse("2021-02-28", formatter), LocalDate.parse("2021-03-08", formatter), "NEW", customer5, null
    private Long id;
    private LocalDate orderDate;
    private LocalDate notknown;
    private String type;
    private Customer customer;
    private String empty;

    public Order(Long id, LocalDate orderDate, LocalDate notknown, String type, Customer customer, String empty) {
        this.id = id;
        this.orderDate = orderDate;
        this.notknown = notknown;
        this.type = type;
        this.customer = customer;
        this.empty = empty;
    }

    private List<Product> products = new ArrayList<>();
}
