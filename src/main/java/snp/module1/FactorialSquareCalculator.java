package snp.module1;

import java.util.concurrent.RecursiveTask;

public class FactorialSquareCalculator extends RecursiveTask<Integer> {

    private Integer n;

    public FactorialSquareCalculator(Integer n) {
        this.n = n;
    }

    @Override
    protected Integer compute() {
        if (n <=1) {
            return  n;
        }
        FactorialSquareCalculator cal = new FactorialSquareCalculator(n-1);
        cal.fork();

        return n * cal.join();
    }
}
