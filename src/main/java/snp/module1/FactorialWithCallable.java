package snp.module1;

import java.util.concurrent.Callable;
import java.util.stream.IntStream;

public class FactorialWithCallable implements Callable<Integer> {

    private Integer n;

    public FactorialWithCallable(Integer n) {
        this.n = n;
    }

    @Override
    public Integer call() throws Exception {
        return IntStream.rangeClosed(2, n).reduce(1, (x, y) -> x * y);
    }
}
