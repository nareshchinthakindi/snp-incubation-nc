package snp.module1;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

public class SquareCalculatorFuture {

    private final ExecutorService executorService = Executors.newSingleThreadExecutor();

    public Future<Integer> calculate(Integer input) {
        return executorService.submit(() -> {
            TimeUnit.SECONDS.sleep(1);
                return input * input;
        });
    }

    public Future<Integer> calculateWithoutSleep(Integer input) {
        return executorService.submit(() -> {
            return input * input;
        });
    }
}
