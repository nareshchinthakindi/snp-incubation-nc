package snp.module1;

public class MultiThreadingMain {

    public static void main(String[] args) {

        Thread t1 = new MyThread1();
        Thread t2 = new MyThread2();

        t1.start();
        t2.start();

    }

}
