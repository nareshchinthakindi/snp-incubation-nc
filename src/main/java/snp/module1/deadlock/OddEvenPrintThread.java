package snp.module1.deadlock;

public class OddEvenPrintThread implements Runnable{


    private final int reminder;

    private final static Object lock = new Object();

    private static int counter = 1;

    public OddEvenPrintThread(int i) {
        this.reminder = i;
    }

    private static final int MAX_VAL=10;

    @Override
    public void run() {

        while (counter < MAX_VAL) {
        synchronized (lock) {

            if (counter%2 != this.reminder) {
                try {
                    lock.wait();
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }

            System.out.println("Thread Name " + Thread.currentThread().getName() + " Counter " + counter);
            counter++;
            lock.notifyAll();

        }
    }
    }

    public static void main(String[] args) {
        Runnable r1 = new OddEvenPrintThread(1);
        Runnable r2 = new OddEvenPrintThread(0);

        Thread t1 = new Thread(r1, "Odd ");
        Thread t2 = new Thread(r2, "Even ");


        t1.start();
        t2.start();
    }
}
