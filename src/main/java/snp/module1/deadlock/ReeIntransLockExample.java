package snp.module1.deadlock;

import java.util.concurrent.locks.ReentrantLock;

public class ReeIntransLockExample {

    private static ReentrantLock reentrantLock = new ReentrantLock();
    static String e = null;

    public static void main(String[] args) {



        Thread t1 = new Thread(() -> accessMethod()); t1.start();

    }

    private static String callAnotherMethod(String e) {
        return null;
    }

    private static void accessMethod() {

        reentrantLock.lock();

        reentrantLock.unlock();
    }
}
