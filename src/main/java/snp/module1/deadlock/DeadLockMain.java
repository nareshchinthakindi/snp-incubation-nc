package snp.module1.deadlock;

import java.util.concurrent.TimeUnit;

public class DeadLockMain {

    public static void main(String[] args) {
        String resource1 = "resource1";
        String resource2 = "resource2";
        Runnable r1 = () -> {
            synchronized (resource1) {
                System.out.println("Resource 1 is locked "+Thread.currentThread().getName());
                try {
                    TimeUnit.SECONDS.sleep(1);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }

                synchronized (resource2) {
                    System.out.println("Resource 2 is locked "+Thread.currentThread().getName());
                }
            }
        };

        Runnable r2 = () -> {
            synchronized (resource2) {
                System.out.println("Resource 2 is locked " + Thread.currentThread().getName());
                try {
                    TimeUnit.SECONDS.sleep(1);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                // } comment it out to fix it
                synchronized (resource1) {
                    System.out.println("Resource 1 is locked " + Thread.currentThread().getName());
                }
            }
        };


        Thread t1 = new Thread(r1, "R1 Thread");
        Thread t2 = new Thread(r2,"R2 Thread");

        t1.start();
        t2.start();


    }
}
