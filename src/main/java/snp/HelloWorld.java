package snp;

import java.util.*;
import java.util.stream.Collectors;

public class HelloWorld {
    public static void main(String[] args) {


        int [] array = {1, 2, 3, 4, 5};

        Map<Integer, List<Integer[]>> values = new HashMap<>();

        for (int i =0; i<array.length; i++) {

                for (int j =i+1; j < array.length; j++) {
                    int sum = array[i] + array[j];

                    values.putIfAbsent(sum, new ArrayList<>());

                    List<Integer[]> pairs = values.get(sum);
                    pairs.add(new Integer[]{array[i] , array[j]});

                }

        }

     Set<List<Integer[]>> listSet =  values.values().stream().filter(obj -> obj.size() > 1).collect(Collectors.toSet());

        for (List<Integer[]> pairs : listSet) {
            pairs.forEach(pair -> System.out.println(Arrays.toString(pair)));

            int sum = pairs.get(0)[0]+pairs.get(0)[1];
            System.out.println(sum);

        }




    }

}
