package snp.designpatterns.factory;

public interface Notification {

   void  notifyUsers();
}
