package snp.designpatterns.factory;

public class EmailNotification implements Notification{

    @Override
    public void notifyUsers() {
        System.out.println("Inside Email Notification");
    }
}
