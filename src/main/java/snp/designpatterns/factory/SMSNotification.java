package snp.designpatterns.factory;

public class SMSNotification implements Notification{


    @Override
    public void notifyUsers() {
        System.out.println("SMS Notification");
    }
}
