package snp.designpatterns.observer.observable;

import snp.designpatterns.observer.obs.NotificationAlertObserver;

import java.util.ArrayList;
import java.util.List;

public class IphoneObservableImpl implements StockObservable{

    List<NotificationAlertObserver> observers = new ArrayList<>();
    public Integer stockCount = 0;

    @Override
    public void add(NotificationAlertObserver notificationAlertObserver) {
          observers.add(notificationAlertObserver);
    }

    @Override
    public boolean remove(NotificationAlertObserver notificationAlertObserver) {
        return observers.remove(notificationAlertObserver);
    }

    @Override
    public void setStockCount(Integer count) {

        if (stockCount == 0) {
            notifyObs();
        }

        stockCount = stockCount + count;
    }

    @Override
    public Integer getStockCount() {
        return stockCount;
    }

    @Override
    public void notifyObs() {
        for (NotificationAlertObserver ob :
        observers ) {
            ob.update();
        }
    }
}
