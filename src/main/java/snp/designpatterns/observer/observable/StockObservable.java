package snp.designpatterns.observer.observable;

import snp.designpatterns.observer.obs.NotificationAlertObserver;

public interface StockObservable {

    void add(NotificationAlertObserver notificationAlertObserver);
    boolean remove(NotificationAlertObserver notificationAlertObserver);
    void setStockCount(Integer count);
    Integer getStockCount();
    void notifyObs();
}
