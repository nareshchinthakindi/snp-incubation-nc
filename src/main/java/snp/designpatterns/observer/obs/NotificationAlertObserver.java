package snp.designpatterns.observer.obs;

public interface NotificationAlertObserver {

    void update();
}
