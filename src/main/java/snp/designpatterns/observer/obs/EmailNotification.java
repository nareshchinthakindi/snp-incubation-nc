package snp.designpatterns.observer.obs;

import snp.designpatterns.observer.observable.StockObservable;

public class EmailNotification implements NotificationAlertObserver{

    private final String email;
    private final StockObservable stockObservable;

    public EmailNotification(String email, StockObservable stockObservable) {
        this.email = email;
        this.stockObservable = stockObservable;
    }
    @Override
    public void update() {
        System.out.println("Email Sending "+email+" count "+stockObservable.getStockCount());
    }
}
