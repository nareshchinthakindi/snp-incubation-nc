package snp.designpatterns.observer.obs;

import snp.designpatterns.observer.observable.StockObservable;

public class MobileNotification implements NotificationAlertObserver{

    private final String userName;
    private final StockObservable stockObservable;

    public MobileNotification(String userName, StockObservable stockObservable) {
        this.userName = userName;
        this.stockObservable = stockObservable;
    }
    @Override
    public void update() {
        System.out.println("Notification "+userName+" count "+stockObservable.getStockCount());
    }
}
