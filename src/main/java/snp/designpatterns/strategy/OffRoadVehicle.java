package snp.designpatterns.strategy;

import snp.designpatterns.strategy.pattern.NormalDriveStrategy;

public class OffRoadVehicle extends Vehicle{

    OffRoadVehicle() {
        super(new NormalDriveStrategy());
    }
}
