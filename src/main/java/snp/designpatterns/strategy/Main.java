package snp.designpatterns.strategy;

public class Main {
    public static void main(String[] args) {
        Vehicle v1 = new SportsVehicle();
        v1.drive();
    }
}
