package snp.designpatterns.strategy;

import snp.designpatterns.strategy.pattern.SportsDriveStrategy;

public class SportsVehicle extends Vehicle{
    SportsVehicle() {
        super(new SportsDriveStrategy());
    }
}
