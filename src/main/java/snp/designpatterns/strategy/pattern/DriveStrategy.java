package snp.designpatterns.strategy.pattern;

public interface DriveStrategy {
   void drive();
}
