package snp.designpatterns.strategy.pattern;

public class NormalDriveStrategy implements DriveStrategy{
    @Override
    public void drive() {
        System.out.println("I am in NormalDriveStrategy");
    }
}
