package snp.designpatterns.strategy.pattern;

public class SportsDriveStrategy implements DriveStrategy{
    @Override
    public void drive() {
        System.out.println("I am in SportsDriveStrategy");
    }
}
