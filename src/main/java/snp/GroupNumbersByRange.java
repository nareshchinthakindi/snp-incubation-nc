package snp;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class GroupNumbersByRange {
    public static void main(String[] args) {
        List<Integer> list = List.of(2, 34, 54, 23, 33, 20, 59, 11, 19, 37 );

        Map<Integer, List<Integer>> values = list.stream().collect(Collectors.groupingBy(i -> i/10*10));

        System.out.println(values);

    }
}
