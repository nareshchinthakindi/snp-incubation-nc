package snp;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class PullIntegersFromStringValues {


    public static void main(String[] args) {
        String[] array = {"as", "123", "32", "2as"};

        List<Integer> values = Arrays.stream(array).filter(s -> s.matches("\\d*")).map(Integer::valueOf).collect(Collectors.toList());
        System.out.println(values);
    }


}
