package snp;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class SplitListToTwoList {

    public static void main(String[] args) {
        List<Integer> list = List.of(2, 34, 54, 23, 33, 20, 59, 11, 19, 37 );
        Map<Boolean, List<Integer>> splitValues = list.stream().collect(Collectors.partitioningBy(x -> x%2 ==0));
        System.out.println(splitValues.get(Boolean.TRUE));
        System.out.println(splitValues.get(Boolean.FALSE));
    }
}
