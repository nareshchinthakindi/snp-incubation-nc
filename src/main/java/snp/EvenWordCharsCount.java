package snp;

public class EvenWordCharsCount {

    public static void main(String[] args) {
        String S = "acbcbba";

        int [] arr = new int[26];

        for (char c : S.toCharArray()) {
              arr[c - 'a']++;
        }

        int count = 0;

        for (int a : arr) {
            if (a %2 == 1) {
                count++;
            }
        }

        System.out.println(count);
    }
}
