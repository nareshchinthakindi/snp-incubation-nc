package snp.mock1;

import java.io.Serializable;


public class Employee implements Serializable {

    private Integer depId;
    private String departmentName;
    private Integer salary;

    public Employee(Integer depId, String departmentName, Integer salary) {
        this.depId = depId;
        this.departmentName = departmentName;
        this.salary = salary;
    }

    public Integer getDepId() {
        return depId;
    }

    public void setDepId(Integer depId) {
        this.depId = depId;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public Integer getSalary() {
        return salary;
    }

    public void setSalary(Integer salary) {
        this.salary = salary;
    }
}
