package snp.mock1;

public class ThreadRunnableTest implements Runnable{

    private static int counter =1;

    private static final int max = 10;
    private static final Object lock = new Object();

    private final int reminder;

    public ThreadRunnableTest(int reminder) {
        this.reminder = reminder;
    }
    @Override
    public void run() {
        while (counter <= max) {
            synchronized (lock) {
                   while (counter %3 != this.reminder) {
                       try {
                           lock.wait();
                       } catch (InterruptedException e) {
                          e.printStackTrace();
                       }
                   }
                System.out.println("Thread Name : "+Thread.currentThread().getName()+" Counter : "+counter);
                counter++;
                lock.notifyAll();
            }
        }
    }
}
