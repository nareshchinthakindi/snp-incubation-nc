package snp.mock1;


import java.util.*;
import java.util.stream.Collectors;

public class Demo {

    //Publisher 1 to 10
    //Consumer T1 to T3
    // T1 - 1 , T2- 2, T3- 3, T1-4

    public static void main(String[] args) {


//        Runnable r1 = new ThreadRunnableTest(1);
//        Runnable r2 = new ThreadRunnableTest(2);
//        Runnable r3 = new ThreadRunnableTest(0);
//
//        new Thread(r1, "T1").start();
//        new Thread(r2, "T2").start();
//        new Thread(r3, "T3").start();

        List<Employee> empList  = Arrays.asList (
                new Employee(1, "Computer", 22000)
                ,new Employee(2, "Computer", 12000)
                ,new Employee(3, "Computer", 48000),
                new Employee(4, "Accounts", 35000)
                ,new Employee(5, "Accounts", 27000)
                ,new Employee(6, "Accounts", 50000));

        Map<String, Optional<Employee>> highestSalW = empList.stream()
                .collect(Collectors.groupingBy(Employee::getDepartmentName, Collectors.maxBy(Comparator.comparing(Employee::getSalary))));

        System.out.println("******************With Comparator ***************");
        highestSalW.forEach((k, v) -> {
            System.out.println("Department : "+k +" sal :"+v.get().getSalary());
        });

        Map<String, Optional<Employee>> highestSal = empList.stream()
               .collect(Collectors.groupingBy(Employee::getDepartmentName, Collectors.maxBy(
                       (o1, o2) -> o1.getSalary() - o2.getSalary())));
        System.out.println("******************Without Comparator ***************");

        highestSal.forEach((k, v) -> {
            System.out.println("Department : "+k +" sal :"+v.get().getSalary());
        });


//         for(Optional<Employee> employee : highestSal) {
//             employee.ifPresent(value -> System.out.println("Dep : " + value.getDepartmentName() + " Sal :" + value.getSalary()));
//         }

        Map<String, Optional<Employee>> deptMaxSalEmp2 = empList.stream()
                .collect(Collectors.groupingBy(Employee::getDepartmentName,
                        Collectors.reducing((e1, e2) -> e1.getSalary() > e2.getSalary() ? e1 : e2)));

        deptMaxSalEmp2.forEach((k, v) -> {
            System.out.println("Department : "+k +" sal :"+v.get().getSalary());
        });

    Map<String, Optional<Employee>> a = empList.stream().collect(Collectors.groupingBy(Employee::getDepartmentName, Collectors.reducing((o1, o2) ->
            o1.getSalary() > o2.getSalary() ? o1 : o2)));


    }

}
