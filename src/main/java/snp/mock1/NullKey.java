package snp.mock1;

import java.util.HashMap;

public class NullKey{

    public static Object findKey(HashMap<?, ?> map, Object key) {
        Object value;
        for (Object k : map.keySet()) {
            value = map.get(k);

//            Optional.of(key).ifPresentOrElse( x -> "Naresh", () -> "Test");
            if (k.equals(key)) {
                return value;
            } else if (value instanceof HashMap) {
                Object result = findKey((HashMap<?, ?>) value, key);
                if (result != null) {
                    return result;
                }
            }
        }
        return null;
    }
}
