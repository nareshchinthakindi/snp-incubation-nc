package snp.mock1;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class EmployeeHighestSalary {

    public static void main(String[] args) {

        List<Employee> empList  = Arrays.asList (
                new Employee(1, "Computer", 22000)
                ,new Employee(2, "Computer", 12000)
                ,new Employee(3, "Computer", 48000),
                new Employee(4, "Accounts", 35000)
                ,new Employee(5, "Accounts", 27000));

        System.out.println("Test");

     Map<String, Optional<Employee>> a =
             empList.stream().collect(Collectors.groupingBy(Employee::getDepartmentName, Collectors.reducing((o1, o2) -> o1.getSalary() > o2.getSalary() ? o1:o2)));

      a.forEach((k, v) -> {
          System.out.println(k+" "+v.get().getSalary());
      });




    }
}
