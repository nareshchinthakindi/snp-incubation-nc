package snp;

import java.util.Arrays;

public class MaxNumberOfVowels {
    public static void main(String[] args) {
        String s =  "The quick brown fox jumps right over the little lazy dog.";

        Arrays.stream(s.split(" ")).filter(s1 -> s1.replaceAll("[^aeiouAEIOU]", "").length() == 2).forEach(System.out::println);

//        System.out.println("quick".replaceAll("[^aeiouAEIOU]", ""));
    }
}
