package snp.concurency;

import java.util.Collections;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class ConcurrentHashMapVsSynchronizedMap {

    public final static int THREAD_POOL_SIZE = 5;
    public static Map<String, Integer> hashTableObj = null;
    public static Map<String, Integer> syncHashMap = null;
    public static Map<String, Integer> conHashMap = null;
    public static void main(String[] args) throws InterruptedException {
        // Test with Hashtable Object
        hashTableObj = new Hashtable<String, Integer>();
        performTest(hashTableObj);
        // Test with synchronizedMap Object
        syncHashMap = Collections.synchronizedMap(new HashMap<String, Integer>());
        performTest(syncHashMap);
        // Test with ConcurrentHashMap Object
        conHashMap = new ConcurrentHashMap<String, Integer>();
        performTest(conHashMap);
    }
    public static void performTest(final Map<String, Integer> thread) throws InterruptedException {
        System.out.println("Test started for: " + thread.getClass());
        long averageTime = 0;
        for (int i = 0; i < 5; i++) {
            long startTime = System.nanoTime();
            ExecutorService ex = Executors.newFixedThreadPool(THREAD_POOL_SIZE);
            for (int j = 0; j < THREAD_POOL_SIZE; j++) {
                ex.execute(new Runnable() {
                    @SuppressWarnings("unused")
                    @Override
                    public void run() {
                        for (int i = 0; i < 500000; i++) {
                            Integer rdn = (int) Math.ceil(Math.random() * 550000);
                            // Retrieve value. We are not using it anywhere
                            Integer val = thread.get(String.valueOf(rdn));
                            // Put value
                            thread.put(String.valueOf(rdn), rdn);
                        }
                    }
                });
            }
            // Initiates an orderly shutdown in which previously submitted tasks are executed, but no new tasks will be accepted. Invocation
            // has no additional effect if already shut down.
            // This method does not wait for previously submitted tasks to complete execution. Use awaitTermination to do that.
            ex.shutdown();
            // Blocks until all tasks have completed execution after a shutdown request, or the timeout occurs, or the current thread is
            // interrupted, whichever happens first.
            ex.awaitTermination(Long.MAX_VALUE, TimeUnit.DAYS);
            long entTime = System.nanoTime();
            long totalTime = (entTime - startTime) / 1000000L;
            averageTime += totalTime;
            System.out.println("500K entried added/retrieved in " + totalTime + " ms");
        }
        System.out.println("For " + thread.getClass() + " the average time is " + averageTime / 5 + " ms\n");

    }

}
