package snp.concurency;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

public class SemaphoreExample {

    public static void main(String[] args) throws InterruptedException {

        Semaphore semaphore = new Semaphore(1);
        ExecutorService executorService = Executors.newFixedThreadPool(3);

//        IntStream.rangeClosed(1, 10).forEach(i -> executorService.execute(new Task(semaphore)));

        executorService.execute(new Task(semaphore));
        executorService.execute(new Task(semaphore));
        executorService.execute(new Task(semaphore));

        executorService.shutdown();
        executorService.awaitTermination(1, TimeUnit.MINUTES);
    }

    static class Task implements Runnable {

        private static int counter = 1;
        Semaphore semaphore;
        public Task(Semaphore semaphore) {
            this.semaphore = semaphore;
        }

        @Override
        public void run() {

            while (counter <= 10) {
                this.semaphore.acquireUninterruptibly();

                System.out.println("Current Thread " + Thread.currentThread().getName() + " " + counter++);

                this.semaphore.release();
            }

        }
    }
}
