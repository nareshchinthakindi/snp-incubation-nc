package snp;

public class ReverseOnlyLetter {
    public static void main(String[] args) {
        String input = "N$a&re*sh";

        int n = input.length();
       char [] ch = input.toCharArray();

        reverseString(ch);
        System.out.println(new String(ch));
    }

    private static void reverseString(char[] ch) {
        int right = ch.length-1, left = 0;

//        while(i<j){
//            if((int)chars[i]<65||(int)chars[i]>117){
//                i++;
//            }else if((int)chars[j]<65||(int)chars[j]>117){
//                j--;
//            }else{
//                char c = chars[i];
//                chars[i]=chars[j];
//                chars[j]=c;
//                i++;
//                j--;
//            }
//        }

        while (right > left) {

            if (!Character.isAlphabetic(ch[left])) {
                left++;
            } else if (!Character.isAlphabetic(ch[right])) {
                right--;
            } else {
                swapCharacters(ch, right, left);
                left++;
                right--;
            }
        }
    }

    private static void swapCharacters(char[] ch, int right, int left) {
        char temp = ch[right];
        ch[right] = ch[left];
        ch[left] = temp;
    }
}
