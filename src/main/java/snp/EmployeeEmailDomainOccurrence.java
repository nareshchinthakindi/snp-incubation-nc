package snp;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class EmployeeEmailDomainOccurrence {

    public static void main(String[] args) {
        List<Employee> employees = List.of(new Employee(1, "Naresh", "naresh@gmail.com"),
                new Employee(2, "Suresh", "naresh@gmail.com"),
                new Employee(3, "Ramesh", "naresh@yahoo.com"),
                new Employee(4, "Jhon", "naresh@outlook.com"),
                new Employee(5, "bob", "naresh@outlook.com"));


        Map<String, Long> domainOccurrence =employees.stream().collect(Collectors.groupingBy(obj-> obj.getEmail().split("@")[1], Collectors.counting()));

        System.out.println(domainOccurrence);


    }
}
