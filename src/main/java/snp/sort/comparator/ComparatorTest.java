package snp.sort.comparator;

import snp.model.Patient;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class ComparatorTest {

    public static void main(String[] args) {


        Comparator<Patient> patientByName = Comparator.comparing(Patient::getName).reversed();

        List<Patient> patients = Arrays.asList(new Patient("Naresh", "Asia"),new Patient("ZAditya", "G"), new Patient("Aditya", "G"));

        System.out.println("before sort "+patients);
        patients.sort(patientByName);
        System.out.println("After sort "+patients);

    }
}
