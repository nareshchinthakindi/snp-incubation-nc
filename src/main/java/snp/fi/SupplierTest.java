package snp.fi;

import java.util.function.Supplier;

public class SupplierTest {

//    The Supplier Interface does not allow input, it returns a value based on a defined type.
//    It is like a function without parameters, with a return type.

    public static void main(String[] args) {


//Supplier function declarations.
        Supplier<String> textSupplier     = () -> "Hello World";
        Supplier<Integer> numberSupplier   = () -> 1234;
        Supplier<Double>  randomSupplier   = () -> Math.random();
        Supplier<Double>  randomSupplierMR = Math::random; //With Method Reference (MR)

            //Calling Supplier functions.
            System.out.println(textSupplier.get());
            System.out.println(numberSupplier.get());
            System.out.println(randomSupplier.get());
            System.out.println(randomSupplierMR.get());
    }
}
