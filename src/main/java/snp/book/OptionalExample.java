package snp.book;

import java.util.Optional;

public class OptionalExample {

            public static void main(String[] args) {
//                String summary = "";
                Book book = getBook();
            String summary = Optional.ofNullable(book)
                   .flatMap(book1 -> Optional.ofNullable(book.getChapter(10))
                   .orElse(Optional.empty())).flatMap(ch ->Optional.ofNullable(ch.getSummary())
                   .orElse(Optional.empty())).orElse("Summary Not Found").toUpperCase();//Or Empty




                System.out.println("Val "+summary);
    }

    private static Book getBook() {
        return new Book();
    }
}
