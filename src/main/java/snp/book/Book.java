package snp.book;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Optional;

@AllArgsConstructor
@Data
@NoArgsConstructor
public class Book {

    private Chapter chapter;

    public Optional<Chapter> getChapter(int n) {
        return Optional.of(new Chapter());
    }
}
