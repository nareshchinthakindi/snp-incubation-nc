package snp.book;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Optional;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class Chapter {

    private String summary;

    public Optional<String> getSummary() {
        return Optional.of("Story");
    }
}
