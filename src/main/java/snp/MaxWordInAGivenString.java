package snp;

import java.util.Arrays;
import java.util.Comparator;

public class MaxWordInAGivenString {
    public static void main(String[] args) {
        String s = "I am interested123455 to grow in my organization";

        System.out.println(Arrays.stream(s.split(" ")).sorted(Comparator.comparing(String::length).reversed()).findFirst().orElse("Not found"));

        System.out.println(Arrays.stream(s.split(" ")).max(Comparator.comparing(String::length)).orElse("Not found"));

        System.out.println(Arrays.stream(s.split(" ")).reduce("", (a, b) -> a.length() > b.length() ? a : b));

    }
}
