package snp;

import snp.model.Patient;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class GenericTest {

    public static void main(String[] args) {


        List<Patient> list = List.of(new Patient("Naresh", "Asia"), new Patient("David", "North America"),
                new Patient("Ramesh", "Asia"), new Patient("Jhon", "North America"));

        Map<String, List<Patient>> patientsByRegion = list.stream().collect(Collectors.groupingBy(Patient::getRegion));

        System.out.println(patientsByRegion);


    }
}
