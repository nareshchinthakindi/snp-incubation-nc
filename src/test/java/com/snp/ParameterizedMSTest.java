package com.snp;

import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ParameterizedMSTest {

    @ParameterizedTest
    @MethodSource("testData")
    @RepeatedTest(3)
    public void myTestMethod(int input, int expectedOutput) {
        int actualOutput = myMethod(input);
        assertEquals(expectedOutput, actualOutput);
    }

    private int myMethod(int input) {
        // implementation of method to test
        return input * 2;
    }

    static Stream<Arguments> testData() {
        return Stream.of(
                Arguments.of(1, 2),
                Arguments.of(2, 4),
                Arguments.of(3, 6)
        );
    }
}
