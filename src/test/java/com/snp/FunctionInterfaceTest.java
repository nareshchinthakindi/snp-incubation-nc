package com.snp;

import org.junit.jupiter.api.Test;

import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.UnaryOperator;

public class FunctionInterfaceTest {

    //FunctionInterface function declarations. (Input Type, Return Type)
    Function<String, String> toUpperCase = (text) -> text.toUpperCase();
    Function<String, String>  toLowerCase = (text) -> text.toLowerCase();
    Function<Integer, Double> log10       = (number) -> Math.log10(number);
    //Method Reference Declarations (Input Type, Return Type)
    Function<String, String>  toUpperCaseMR = String::toUpperCase;
    Function<String, String>  toLowerCaseMR = String::toLowerCase;
    Function<Integer, Double> log10MR       = Math::log10;
    //BiFunction Example (Input Type, Input Type, Return Type)
    BiFunction<Integer, Integer, Integer> powerOf = (base, power) -> (int) Math.pow(base, power);
    //UnaryOperator Example (Input and Return type are same.)
    UnaryOperator<Integer> appendText = (text) -> text * text;

//    The Function Interface takes an input, it returns a defined type. It is like a function with a parameter, with a return type.
//    The first declaration is input, the second is the return type. The BiFunction Interface takes two inputs rather than one input.
//    That’s the only difference between Function and BiFunction interfaces. Also, UnaryOperator interface takes and returns the same type.
//
//    If we chain the functions with andThen method, the execution order will be like the left to right flow.
//    First, the first function will be run, then the others will be run. If we want to run these functions right to left we can use compose method rather than the andThen method.

    @Test
    public void test2() {

        String chainResult1 = toUpperCase.andThen(toLowerCase).apply("heLLo sW teSt ACadEmy!");
        String chainResult2 = toLowerCase.andThen(toUpperCase).apply("heLLo sW teSt ACadEmy!");
        System.out.println(chainResult1);
        System.out.println(chainResult2);
    }
}
