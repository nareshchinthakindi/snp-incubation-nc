package com.snp;

import org.junit.jupiter.api.Test;

import java.util.StringJoiner;

public class StringJoinerTest {

    @Test
    public void test() {

        StringJoiner sj = new StringJoiner(",", "[", "]");

        sj.add("Naresh").add("ROhini").add("isha").add("Swapna");

        System.out.println(sj);

    }
}
