package com.snp;

import org.junit.jupiter.api.Test;

import java.util.function.BiPredicate;
import java.util.function.Predicate;

public class PredicateTest {

//    The predicate takes an input, it returns a boolean value as true or false.
//    It is like a function with a parameter, with the boolean return type. BiPredicate Interface takes two inputs and returns a boolean value.
//
//    Negate method does the inversion for the value.
//
//    And method is working as logical AND operation.
//
//    Or method is working as a logical OR operation.

    //Predicate function declaration.
    String            sampleText        = "Hello SW Test Academy";
    Predicate<String> containsPredicate = (text) -> sampleText.contains(text);
    //BiPredicate function declaration.
    BiPredicate<String, String> containsBiPredicate   = (text, pattern) -> text.contains(pattern);
    BiPredicate<String, String> containsBiPredicateMR = String::contains; //Method reference version.


    @Test
    public void predicateTest() {

        //Calling Predicate functions.
        boolean result = containsPredicate.test("SW");
        boolean resultOfNegate = containsPredicate.negate().test("SW"); //negate is inverse operation like "does not contain".
        boolean andResult = containsBiPredicate.and(containsBiPredicate.negate()).test("SW", "SW"); //Logical AND operation.
        boolean orResult = containsBiPredicate.or(containsBiPredicate.negate()).test("SW", "SW"); //Logical OR operation.
        System.out.println(result);
        System.out.println(resultOfNegate);
        System.out.println(andResult);
        System.out.println(orResult);

    }
}
