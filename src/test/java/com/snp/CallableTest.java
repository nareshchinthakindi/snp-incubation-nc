package com.snp;

import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;

import java.util.concurrent.Callable;
import java.util.function.Supplier;

public class CallableTest {

    Callable<Double> callFunction     = () -> Math.random() * 100;
    Supplier<Double> supplierFunction = () -> Math.random() * 100;
    @SneakyThrows
    @Test
    public void callableTest() throws Exception {
        //Calling functions.
        int callResult = callFunction.call().intValue();
        int getResult = supplierFunction.get().intValue();
        System.out.println(callResult);
        System.out.println(getResult);
    }
}
