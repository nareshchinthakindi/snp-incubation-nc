package com.snp.module1.completablefuture;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.atomic.AtomicInteger;

public class CompletableFutureExample {
//    private static final AtomicInteger counter = new AtomicInteger(1);
    public static void main(String[] args) {


       //CompletableFuture.allOf();
            for (int i = 0; i < 100; i++) {
                CompletableFuture.supplyAsync(CompletableFutureExample::getOrderId)
                        .thenApply(CompletableFutureExample::enrichDetails)
                        .thenAccept(CompletableFutureExample::email);
            }
    }
    private static AtomicInteger getOrderId() {
//        System.out.println("Counter "+counter.getAndIncrement());
        return new AtomicInteger(1);
    }
    private static AtomicInteger enrichDetails(AtomicInteger id) {
//        System.out.println("Counter "+counter.getAndIncrement());
        System.out.println("enrichDetails "+id.getAndIncrement());
        return id;
    }

    private static void email(AtomicInteger id) {
//        System.out.println("Counter "+counter.getAndIncrement());
        System.out.println("email "+id.getAndIncrement());
    }



//
//    private static AtomicInteger performPayment(AtomicInteger id) {
//        System.out.println("Counter "+counter.getAndIncrement());
//        System.out.println("performPayment "+id.getAndIncrement());
//        return id;
//    }
//    private static AtomicInteger dispatchOrder(AtomicInteger id) {
//        System.out.println("Counter "+counter.getAndIncrement());
//        System.out.println("dispatchOrder "+id.getAndIncrement());
//        return id;
//    }
}
