package com.snp.module1;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import snp.module1.FactorialWithCallable;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class FactorialWithCallableTest {

    @Test
    public void callTest() throws Exception {

        System.out.println("Core "+Runtime.getRuntime().availableProcessors());
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        Future<?> future = executorService.submit(new FactorialWithCallable(5));
        executorService.shutdown();
        Assertions.assertEquals(120, future.get());
        Assertions.assertTrue(future.isDone());

    }
}
