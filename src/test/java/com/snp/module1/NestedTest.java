package com.snp.module1;

import org.junit.jupiter.api.*;
import snp.module1.MathUtils;

@DisplayName("Nested Display Name")
public class NestedTest {


    private MathUtils mathUtils = null;



    @BeforeEach
    public void init() {
        mathUtils = new MathUtils();
    }

    @Nested
    @DisplayName("AddTest")
    class AddTest {
        @Test
        @DisplayName("Add Test")
        public void addTest() {
            Assertions.assertEquals(2, mathUtils.add(1, 1));
        }

        @Test
        @DisplayName("Neg Test")
        public void negTest() {
            Assertions.assertEquals(-2, mathUtils.add(-1, -1));
        }
    }

    @Test
    @DisplayName("Add Test M")
    public void addTest() {
        Assertions.assertEquals(2, mathUtils.add(1, 1));
    }

    @Test
    @DisplayName("Neg Test M")
    public void negTest() {
        Assertions.assertEquals(-2, mathUtils.add(-1, -1));
    }
}
