package com.snp.module1;

import org.junit.jupiter.api.Test;
import snp.module1.EventLoggingRunnable;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class EventLoggingRunnableTest {

    @Test
    public void runTest() {
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        Future<?> future = executorService.submit(new EventLoggingRunnable());
        executorService.shutdown();
    }
}
