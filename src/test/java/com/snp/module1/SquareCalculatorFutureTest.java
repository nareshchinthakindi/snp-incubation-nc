package com.snp.module1;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import snp.module1.SquareCalculatorFuture;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class SquareCalculatorFutureTest {



    @Test
    public void squareFutureTest() throws ExecutionException, InterruptedException {
        SquareCalculatorFuture scf = new SquareCalculatorFuture();

       Future<Integer> future = scf.calculate(10);

        while(!future.isDone()) {
            System.out.println("Calculating...");
            TimeUnit.SECONDS.sleep(1);
        }

        Assertions.assertEquals(100, future.get());

    }

    @Test
    public void squareFutureWithoutSleepTest() throws ExecutionException, InterruptedException, TimeoutException {
        SquareCalculatorFuture scf = new SquareCalculatorFuture();
        Future<Integer> future = scf.calculateWithoutSleep(10);
        Assertions.assertEquals(100, future.get(500, TimeUnit.MICROSECONDS));
    }

    @Test
    public void futureCancelTest() throws ExecutionException, InterruptedException, TimeoutException {
        SquareCalculatorFuture scf = new SquareCalculatorFuture();
        Future<Integer> future = scf.calculateWithoutSleep(10);
        Assertions.assertFalse(future.isCancelled());
        boolean cancel =  future.cancel(true);
        System.out.println(cancel);
        Assertions.assertTrue(future.isCancelled());
    }
}
