package com.snp.module1.excutableservice;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class ExecutorServiceExample {

    public static void main(String[] args) {
        ExecutorService executor = Executors.newCachedThreadPool();

//        Executors.newCachedThreadPool();
//        Executors.newScheduledThreadPool(10);
//        Executors.newSingleThreadExecutor();

      List<Callable<Integer>> callableList = Arrays.asList(()-> 1, ()-> 2, ()->3 );

        try {

      List<Future<Integer>> integerFuture = executor.invokeAll(callableList);

      Integer sum = integerFuture.stream().map(f -> {
          try {
              return  f.get();
          } catch (Exception e) {
             throw new IllegalStateException(e);
          }
      }).mapToInt(Integer::intValue).sum();

            System.out.println(sum);
        } catch (InterruptedException e) {// thread was interrupted
            e.printStackTrace();
        } finally {

            // shut down the executor manually
            executor.shutdown();

        }
    }
}
