package com.snp.module1.excutableservice;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class CachedExecutorService {

    public static void main(String[] args) {

        ExecutorService executorService = Executors.newCachedThreadPool();

        for (int i = 0; i<20; i++) {
            int finalI = i;
            Runnable r3 = () -> System.out.println("Hello "+ finalI+" "+Thread.currentThread().getName());
            executorService.submit(r3);
        }

        executorService.shutdown();

    }
}
