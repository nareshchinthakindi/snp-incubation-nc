package com.snp.module1.excutableservice;

import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class ScheduledPoolTypeService {

    public static void main(String[] args) {

        ScheduledThreadPoolExecutor threadPool
                = new ScheduledThreadPoolExecutor(2);
        for (int i = 0; i<20; i++) {
            int finalI = i;
            Runnable r3 = () -> System.out.println("Hello "+ finalI+" "+Thread.currentThread().getName());
            threadPool.scheduleWithFixedDelay(r3, 10, 1, TimeUnit.SECONDS);
        }


    }
}
