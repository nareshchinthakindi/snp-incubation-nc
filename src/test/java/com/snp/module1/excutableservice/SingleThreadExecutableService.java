package com.snp.module1.excutableservice;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class SingleThreadExecutableService {

    public static void main(String[] args) {
        Runnable r1 = () -> System.out.println("Naresh");

        Runnable r2 = () -> System.out.println("Suresh");

        ExecutorService executorService = Executors.newSingleThreadExecutor();

        executorService.submit(r1);
        executorService.submit(r2);
        executorService.shutdown();
    }
}
