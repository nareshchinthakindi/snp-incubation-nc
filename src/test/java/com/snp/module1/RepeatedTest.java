package com.snp.module1;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.RepetitionInfo;
import snp.module1.MathUtils;

import java.util.Random;

public class RepeatedTest {


    private MathUtils mathUtils = null;



    @BeforeEach
    public void init() {
        mathUtils = new MathUtils();
    }

   @org.junit.jupiter.api.RepeatedTest(3)//Repeated 3 times
    public void assertAllTest() {

        Assertions.assertAll( () -> Assertions.assertThrows(ArithmeticException.class, () -> mathUtils.divide(1, 0), "Divide should throw an Arithmetic Exception"),
                () -> Assertions.assertEquals(2, mathUtils.divide(2, 1))
        );

    }

    @org.junit.jupiter.api.RepeatedTest(3)//Repeated 3 times
    public void repeated(RepetitionInfo repetitionInfo) {

        System.out.println("RepetitionInfo info "+repetitionInfo.getTotalRepetitions()+" Current "+repetitionInfo.getCurrentRepetition());
       int a = new Random(10).nextInt();
       int b = new Random(10).nextInt();
      int actual = mathUtils.add(a, b);

      Assertions.assertEquals(a + b,actual) ;
    }
}
