package com.snp.module1;

import org.junit.jupiter.api.*;
import org.junit.jupiter.api.condition.EnabledOnOs;
import org.junit.jupiter.api.condition.OS;
import snp.module1.MathUtils;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class MathUtilsTest {

    private MathUtils mathUtils = null;

    @BeforeAll
    static public void beforeAll() {
        System.out.println("Need to be static because need to be call before class instance is created");
    }


    @BeforeEach
    public void init() {
        mathUtils = new MathUtils();
    }

    @Test
    @DisplayName("Divide ArithmeticException")
    @EnabledOnOs(OS.WINDOWS)
    public void exceptionTest() {
        Assumptions.assumeTrue(true);//This means that external service is up and running if not don't run and give me wrong report.
        Assertions.assertThrows(ArithmeticException.class, () -> mathUtils.divide(1, 0), () -> "Divide should throw an Arithmetic Exception"); // String would get calculated only when test fails
    }

    @Test
    @DisplayName("Divide ArithmeticException")
    @EnabledOnOs(OS.WINDOWS)
    public void assertAllTest() {

        Assertions.assertAll( () -> Assertions.assertThrows(ArithmeticException.class, () -> mathUtils.divide(1, 0), "Divide should throw an Arithmetic Exception"),
                () -> Assertions.assertEquals(1, mathUtils.divide(2, 1))
        );

    }

    @Test
    @Disabled
    public void test() {
    }

    @AfterEach
    public void tear() {
        System.out.println("Cleaning-----");
        mathUtils = null;
    }

    @AfterAll
    static public void afterAll() {
        System.out.println("Cleanup After all");
    }

}
