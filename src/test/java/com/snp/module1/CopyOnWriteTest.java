package com.snp.module1;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class CopyOnWriteTest {


    @Test
    public void basicTest() {
        CopyOnWriteArrayList<Integer> copyOnWriteArrayList = new CopyOnWriteArrayList<>(new Integer[]{1, 3, 5, 8});

        Iterator<Integer> iterator = copyOnWriteArrayList.iterator();

        copyOnWriteArrayList.add(23);
        List<Integer> result = new LinkedList<>();
        iterator.forEachRemaining(result::add);
        Assertions.assertArrayEquals(new int[]{1, 3, 5, 8}, result.stream()
                .mapToInt(Integer::intValue)
                .toArray());

        result.clear();
        iterator = copyOnWriteArrayList.iterator();
        iterator.forEachRemaining(result::add);
        Assertions.assertArrayEquals(new int[]{1, 3, 5, 8, 23}, copyOnWriteArrayList.stream()
                .mapToInt(Integer::intValue)
                .toArray());
    }
}
