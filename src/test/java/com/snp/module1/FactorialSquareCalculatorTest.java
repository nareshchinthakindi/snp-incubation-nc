package com.snp.module1;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import snp.module1.FactorialSquareCalculator;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;

public class FactorialSquareCalculatorTest {


    @Test
    public void factForkSqTest() throws ExecutionException, InterruptedException {
       ForkJoinPool fjp = new ForkJoinPool();
       FactorialSquareCalculator cal = new FactorialSquareCalculator(5);
       ForkJoinTask<Integer> val =  fjp.submit(cal);
       Assertions.assertEquals(120, val.get());
    }

}
