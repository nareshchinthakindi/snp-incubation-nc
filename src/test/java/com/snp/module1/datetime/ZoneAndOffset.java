package com.snp.module1.datetime;

import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;

public class ZoneAndOffset {


    @Test
    public void allZones() {
        System.out.println(ZoneId.getAvailableZoneIds());
    }

    @Test
    public void updateLocalTimeToDifferentTimeZone() {
        ZoneId zoneId = ZoneId.of("Europe/Paris");

        ZonedDateTime zonedDateTime = ZonedDateTime.of(LocalDateTime.now(), zoneId);

        System.out.println(zonedDateTime);
    }

}
