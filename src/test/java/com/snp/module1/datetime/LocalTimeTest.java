package com.snp.module1.datetime;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.LocalTime;
import java.time.temporal.ChronoUnit;

public class LocalTimeTest {


    @Test
    public void test() {
        LocalTime now = LocalTime.now();
        System.out.println(now);

        now = LocalTime.parse("06:30");
        System.out.println(now);

        LocalTime sixThirty = LocalTime.of(6, 30);
//Add hour
        LocalTime sevenThirty = LocalTime.parse("06:30").plus(1, ChronoUnit.HOURS);

        System.out.println(sevenThirty);

        int six = LocalTime.parse("06:30").getHour();

        boolean before = LocalTime.parse("06:30").isBefore(LocalTime.parse("07:30"));
        Assertions.assertTrue(before);
    }

}
