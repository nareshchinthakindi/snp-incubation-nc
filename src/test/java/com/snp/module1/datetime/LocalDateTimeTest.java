package com.snp.module1.datetime;

import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.time.Month;

public class LocalDateTimeTest {

    @Test
    public void nowTest() {
        System.out.println(LocalDateTime.now());

        System.out.println(LocalDateTime.of(2015, Month.FEBRUARY, 20, 06, 30));
        System.out.println(LocalDateTime.parse("2015-02-20T06:30:00"));
    }
}
