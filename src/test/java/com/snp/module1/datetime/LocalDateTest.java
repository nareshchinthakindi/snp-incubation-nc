package com.snp.module1.datetime;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

public class LocalDateTest {


    @Test
    public void dateTest() {

        LocalDate localDate = LocalDate.now();

        System.out.println(localDate);

        System.out.println(LocalDate.of(2015, 02, 20));

        System.out.println(LocalDate.parse("2015-02-20"));

        System.out.println(LocalDate.now().plusDays(1));//Tomorrow

        //one month back
        System.out.println("One Month Back : "+ LocalDate.now().minus(1, ChronoUnit.MONTHS));

        System.out.println(" Month Back For Feb is different "+ LocalDate.parse("2016-03-31").minus(1, ChronoUnit.MONTHS));
    }

    @Test
    public void weekTest() {
        DayOfWeek sunday = LocalDate.parse("2023-04-24").getDayOfWeek();

        System.out.println(sunday.getValue());//1-Monday and 7-Sunday

        int twelve = LocalDate.parse("2016-06-12").getDayOfMonth();

        Assertions.assertEquals(12, twelve);
    }

    @Test
    public void beforeAfterTest() {
        boolean notBefore = LocalDate.parse("2016-06-12")
                .isBefore(LocalDate.parse("2016-06-11"));

        Assertions.assertFalse(notBefore);

        boolean isAfter = LocalDate.parse("2016-06-12")
                .isAfter(LocalDate.parse("2016-06-11"));

        Assertions.assertTrue(isAfter);
    }
}
