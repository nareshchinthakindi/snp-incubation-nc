package com.snp.module1;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import snp.module1.CallableExample;

import java.util.concurrent.Callable;

public class CallableExampleTest {


    @Test
    public void callTest() throws Exception {

        Callable<Integer> ce = new CallableExample();

        Integer result = ce.call();
       Assertions.assertTrue(result >=0 && result <=10);

    }
}
