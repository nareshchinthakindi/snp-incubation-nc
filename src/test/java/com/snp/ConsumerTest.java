package com.snp;

import org.junit.jupiter.api.Test;

import java.util.function.BiConsumer;
import java.util.function.Consumer;

public class ConsumerTest {

//    The Consumer Interface takes an input, it does not return a value. It is like a function with a parameter, without a return type.
//    BiConsumer Interface takes two inputs and does not return anything. That’s why it is called Bi Consumer. If we chain multiple

    Consumer<String> st = (input) -> System.out.println(input.toUpperCase());
    Consumer<Integer> in = (input) -> System.out.println(input + 10);

    BiConsumer<Integer, String> bi = (s1, in1) -> System.out.println(s1 + in1);
//BiConsumer<Integer, Integer> powConsumer = (base, power) -> System.out.println(Math.pow(base, power));

    @Test
    public void test1() {
        st.accept("naresh");
        in.accept(10);
        bi.accept(23, " Naresh");
//        powConsumer.accept(3 ,2);
    }

}
